# coding=utf-8
__author__ = 'BorYee'

from flask import Blueprint

idx = Blueprint('idx',__name__)


@idx.route('/')
def index_page():
    res = '<h1 align="center">API INTRODUCE!</h1><br>' \
          '<p align="center">获取爬取关键词：[GET]</p><br>' \
          '<p align="center">http://boryee.6655.la:5000/api/v0.1/industry/</p><br>' \
          '<p align="center">基于行业的微博检索：[GET]</p><br>' \
          '<p align="center">http://boryee.6655.la:5000/api/v0.1/weibo/industry/社交网络</p><br>' \
          '<p align="center">基于子行业的微博检索：[GET]</p><br>' \
          '<p align="center">http://boryee.6655.la:5000/api/v0.1/weibo/sub_industry/兴趣社区</p><br>' \
          '<p align="center">基于爬取关键词的微博检索：[GET]</p><br>' \
          '<p align="center">http://boryee.6655.la:5000/api/v0.1/weibo/key_word/兴趣社区</p><br>' \
          '<p align="center">基于全文的微博检索：[GET]</p><br>' \
          '<p align="center">http://boryee.6655.la:5000/api/v0.1/weibo/search_weibo/兴趣社区</p><br>' \
          '<p align="center">微博极性分析：[POST]</p><br>' \
          '<p align="center">http://boryee.6655.la:5000/api/v0.1/weibo/polarity/weibo/</p><br>' \
          '<p align="center">POST内容格式如：</p><br>' \
          "data = {<br>" \
          "    0: '【尼玛！千万别惹程序员！后果太可Pia】',<br>" \
          "    1: '广州某IT公司上演一场因男程序员向同性同事表白遭拒，一怒之下清除公司服务器所有数据的闹剧。程序员多为苦逼“技术宅”，通宵熬夜工作乃家常便饭，工作压力大啊。。。记住了！千万别轻易得罪程序员！！！'<br>}"
    return res