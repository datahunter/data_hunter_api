__author__ = 'BorYee'

import MySQLdb


class Config():
    DB_NAME = 'db_weibo'


class BoryeeMacEnv(Config):
    HOST = 'localhost'
    USER = 'root'
    PASSWD = ''
    WORD_TABLE = 'tb_polarity_word_dict'
    EXPR_TABLE = 'tb_polarity_expr_dict'
    WORD_FILE = '../res/expression.txt'
    EXPR_FILE = '../res/words.txt'
    USER_JIEBA_DICT_FILE = '../res/dict_jieba.txt'


config = {
    'boryee_mac': BoryeeMacEnv
}


def export_dict_2_db(config_name):
    profile = config[config_name]
    conn = MySQLdb.connect(host=profile.HOST, user=profile.USER, passwd=profile.PASSWD, db=profile.DB_NAME)
    cursor = conn.cursor()
    table_exist_judge_sql = 'SELECT count(*) FROM information_schema.tables WHERE table_schema = %s AND table_name = %s'
    create_expr_table_sql = 'CREATE TABLE ' + profile.EXPR_TABLE + ' (id int not null primary key auto_increment, word varchar(45),polarity int)'
    create_word_table_sql = 'CREATE TABLE ' + profile.WORD_TABLE + ' (id int not null primary key auto_increment, word varchar(45),polarity int)'
    insert_expr_sql = 'INSERT INTO ' + profile.EXPR_TABLE + ' (word,polarity) VALUES (%s,%s)'
    insert_word_sql = 'INSERT INTO ' + profile.WORD_TABLE + ' (word,polarity) VALUES (%s,%s)'
    if not cursor.fetchmany(cursor.execute(table_exist_judge_sql, [profile.DB_NAME, profile.WORD_TABLE]))[0][0]:
        cursor.execute(create_word_table_sql)
    if not cursor.fetchmany(cursor.execute(table_exist_judge_sql, [profile.DB_NAME, profile.EXPR_TABLE]))[0][0]:
        cursor.execute(create_expr_table_sql)
    conn.commit()
    with open(profile.WORD_FILE, 'r') as f:
        for record in f.readlines():
            record = record.split('\t')
            cursor.execute(insert_expr_sql, [record[0], record[1]])
    with open(profile.EXPR_FILE, 'r') as f:
        for record in f.readlines():
            record = record.split('\t')
            cursor.execute(insert_word_sql, [record[0], record[1]])
    cursor.close()
    conn.commit()
    conn.close()


def export_dict_2_file(config_name):
    profile = config[config_name]
    with open(profile.USER_JIEBA_DICT_FILE, 'w', -1) as f:
        for file_name in [profile.EXPR_FILE, profile.WORD_FILE]:
            with open(file_name, 'r') as src_file:
                for record in src_file.readlines():
                    record = record.replace('\r', '').replace('\n', '')
                    record = record.split('\t')
                    if record[1] == '1':
                        f.write(record[0] + ' 99999999 neg \n')
                    elif record[1] == '0':
                        f.write(record[0] + ' 99999999 pos \n')


if __name__ == '__main__':
    export_dict_2_file('boryee_mac')


