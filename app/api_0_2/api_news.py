# coding=utf-8
__author__ = 'BorYee'

from app import db
from app.api_0_2 import api
from app.models import News
from flask import jsonify, request


@api.route('/news/post_news/', methods=['POST'])
def add_news():
    """
    这个API宝宝是用来接收爬虫端的新闻数据的。
    """
    try:
        news_json = dict(request.form)
        if list(News.query.filter(
                News.title.like('%' + news_json['title'][0] + '%'),
                News.publish_time.like('%' + news_json['publishTime'][0] + '%'),
                News.news_url.like('%' + news_json['url'][0] + '%'),
                News.source.like('%' + news_json['source'][0] + '%')
        ).all()).__len__() == 0:
            news_vo = News(
                title=news_json['title'][0],
                content=news_json['content'][0],
                news_url=news_json['url'][0],
                publish_time=news_json['publishTime'][0],
                source=news_json['source'][0]
            )
            db.session.add(news_vo)
            db.session.commit()
            return jsonify({'code': 200, 'result': 'Success'})
        else:
            return jsonify({'code': 200, 'result': 'News already existed.'})
    except Exception, e:
        print e
        return jsonify({'code': 400, 'result': str(e)})