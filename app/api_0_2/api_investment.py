__author__ = 'BorYee'

from app import db
from app.api_0_2 import api
from app.models import StartupLink, StartupTag, FounderLink, StartupInvestment, InvestFirmLink, NewInvestment
from app.models import StartupInfo
from app.utils import time_proc

from flask import request, jsonify


@api.route('/startup/post_link/', methods=['POST'])
def add_startup_link():
    try:
        link = dict(request.form)
        if list(
                StartupLink.query.filter_by(
                        start_up_link=link['start_up_link'][0]
                ).all()
        ).__len__() == 0:
            for key in link:
                print key, link[key]
            startup_link_vo = StartupLink(
                start_up_name=link['start_up_name'][0],
                start_up_link=link['start_up_link'][0],
                source=link['source'][0]
            )
            db.session.add(startup_link_vo)
            db.session.commit()
            return jsonify({
                'code': '200',
                'result': 'successful'
            })
        else:
            return jsonify({
                'code': 200,
                'result': 'Link has already existed.'
            })
    except Exception, e:
        print e
        return jsonify({'code': 400, 'result': str(e)})


@api.route('/startup/get_link/<string:size>')
def get_startup_link(size):
    size = int(size)
    link = []
    link_list = StartupLink.query.all()
    for record in link_list:
        if size == 0:
            break
        if record.fetch_date is None or time_proc.get_delta_date(record.fetch_date) > 2:
            link.append(record.to_json())
            record.fetch_tag = 1
            record.fetch_date = time_proc.get_current_date()
            db.session.commit()
            size -= 1
    for record in link_list:
        if size == 0:
            break
        if record.fetch_tag is None:
            link.append(record.to_json())
            record.fetch_tag = 1
            record.fetch_date = time_proc.get_current_date()
            db.session.commit()
            size -= 1
    return jsonify({
        'data': link,
        'code': 200,
        'result': 'successful.'
    })


@api.route('/startup/post_info/', methods=['POST'])
def add_startup_info():
    try:
        data = dict(request.form)
        if list(
                StartupInfo.query.filter(
                        StartupInfo.start_up_name.like('%' + data['start_up_name'][0] + '%'),
                        StartupInfo.full_name.like('%' + data['full_name'][0] + '%')
                ).all()
        ).__len__() == 0:
            startup_info_vo = StartupInfo(
                lid=data['lid'][0],
                start_up_name=data['start_up_name'][0],
                area=data['area'][0],
                sub_area=data['sub_area'][0],
                location=data['location'][0],
                sub_location=data['sub_location'][0],
                official_link=data['official_link'][0],
                introduction=data['introduction'][0],
                full_name=data['full_name'][0],
                found_date=data['found_date'][0],
                status=data['status'][0],
                address=data['address'][0],
                mail=data['mail'][0],
                phone=data['phone'][0],
            )
            db.session.add(startup_info_vo)
            db.session.commit()
            return jsonify({
                'code': 200,
                'result': 'successful.'
            })
        else:
            return jsonify({
                'code': 200,
                'result': 'Info has already existed.'
            })
    except Exception, e:
        print e
        return jsonify({
            'code': 400,
            'result': str(e)
        })


@api.route('/startup/post_tag/', methods=['POST'])
def add_startup_tag():
    try:
        data = dict(request.form)
        if list(
                StartupInfo.query.filter_by(lid=data['lid'][0])
        ).__len__() > 0:
            sid = StartupInfo.query.filter_by(lid=data['lid'][0])[0].to_json()['id']
            if list(StartupTag.query.filter_by(sid=sid, tag=data['tag'][0]).all()).__len__() == 0:
                tag_vo = StartupTag(
                    sid=sid,
                    tag=data['tag'][0]
                )
                db.session.add(tag_vo)
                db.session.commit()
                return jsonify({
                    'code': 200,
                    'result': 'successful'
                })
            else:
                return jsonify({
                    'code': 200,
                    'result': 'Tag for this startup has already existed.'
                })
        else:
            return jsonify({
                'code': 200,
                'result': 'Startup does not exist.'
            })
    except Exception, e:
        print e
        return jsonify({
            'code': 400,
            'result': str(e)
        })


@api.route('/startup/add_investment/', methods=['POST'])
def add_investment():
    try:
        data = dict(request.form)
        if list(
                StartupInfo.query.filter_by(lid=data['lid'][0])
        ).__len__() > 0:
            sid = StartupInfo.query.filter_by(lid=data['lid'][0])[0].to_json()['id']
            if list(
                    StartupInvestment.query.filter_by(
                            sid=sid,
                            invest_firm_name=data['invest_firm_name'][0],
                            invest_time=data['invest_time'][0]
                    ).all()).__len__() == 0:
                investment_vo = StartupInvestment(
                    sid=sid,
                    invest_firm_name=data['invest_firm_name'][0],
                    invest_time=data['invest_time'][0],
                    invest_round=data['invest_round'][0],
                    amount=data['amount'][0]
                )
                db.session.add(investment_vo)
                db.session.commit()
                return jsonify({
                    'code': 200,
                    'result': 'successful'
                })
            else:
                return jsonify({
                    'code': 200,
                    'result': 'Investment info for this startup has already existed.'
                })
        else:
            return jsonify({
                'code': 200,
                'result': 'Startup does not exist.'
            })
    except Exception, e:
        print e
        return jsonify({
            'code': 400,
            'result': str(e)
        })


@api.route('/founder/add_link/', methods=['POST'])
def add_founder_link():
    try:
        data = dict(request.form)
        if list(
                StartupInfo.query.filter_by(lid=data['lid'][0])
        ).__len__() > 0:
            sid = StartupInfo.query.filter_by(lid=data['lid'][0])[0].to_json()['id']
            if list(FounderLink.query.filter_by(
                    sid=sid,
                    founder_link=data['founder_link'][0]
            ).all()).__len__() == 0:
                link_vo = FounderLink(
                    sid=sid,
                    founder_name=data['founder_name'][0],
                    founder_link=data['founder_link'][0],
                    source=data['source'][0]
                )
                db.session.add(link_vo)
                db.session.commit()
                return jsonify({
                    'code': 200,
                    'result': 'successful'
                })
            else:
                return jsonify({
                    'code': 200,
                    'result': 'Link has already exist.'
                })
        else:
            return jsonify({
                'code': 200,
                'result': 'Startup dose not exist.'
            })
    except Exception, e:
        print e
        return jsonify({
            'code': 400,
            'result': str(e)
        })


@api.route('/invest_firm/add_link/', methods=['POST'])
def add_invest_firm_link():
    try:
        link = dict(request.form)
        if list(
                InvestFirmLink.query.filter_by(
                        invest_firm_link=link['invest_firm_link'][0]
                ).all()
        ).__len__() == 0:
            link_vo = InvestFirmLink(
                invest_firm_name=link['invest_firm_name'][0],
                invest_firm_link=link['invest_firm_link'][0],
                source=link['source'][0]
            )
            db.session.add(link_vo)
            db.session.commit()
            return jsonify({
                'code': '200',
                'result': 'successful'
            })
        else:
            return jsonify({
                'code': 200,
                'result': 'Link has already existed.'
            })
    except Exception, e:
        print e
        return jsonify({'code': 400, 'result': str(e)})


@api.route('/investment/add_investment/', methods=['POST'])
def add_new_investment():
    try:
        data = dict(request.form)
        if list(
                NewInvestment.query.filter(
                        NewInvestment.invest_firm_name.like('%' + data['invest_firm_name'][0] + '%'),
                        NewInvestment.start_up_name.like('%' + data['start_up_name'][0] + '%'),
                        NewInvestment.invest_time.like('%' + data['invest_time'][0] + '%')
                ).all()
        ).__len__() == 0:
            investment_vo = NewInvestment(
                start_up_name=data['start_up_name'][0],
                area=data['area'][0],
                location=data['location'][0],
                invest_time=data['invest_time'][0],
                amount=data['amount'][0],
                invest_firm_name=data['invest_firm_name'][0],
                round=data['round'][0]
            )
            db.session.add(investment_vo)
            db.session.commit()
            return jsonify({
                'code': '200',
                'result': 'successful'
            })
        else:
            return jsonify({
                'code': 200,
                'result': 'Investment has already existed.'
            })
    except Exception, e:
        print e
        return jsonify({'code': 400, 'result': str(e)})