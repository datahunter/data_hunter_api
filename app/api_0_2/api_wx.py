from app.utils import time_proc

__author__ = 'BorYee'

from app import db
from app.api_0_2 import api
from app.models import Weixin, WeixinIndustryKeyWord
from flask import request, jsonify


@api.route('/wx/post_wx/', methods=['POST'])
def add_wx():
    try:
        wx_json = dict(request.form)
        if list(Weixin.query.filter(
                Weixin.content.like('%' + wx_json['content'][0] + '%'),
                Weixin.content_time.like('%' + wx_json['content_time'][0] + '%'),
                Weixin.content_link.like('%' + wx_json['content_link'][0] + '%')
        ).all()).__len__() == 0:
            wx_vo = Weixin(
                industry=wx_json['industry'][0],
                sub_industry=wx_json['sub_industry'][0],
                key_word=wx_json['key_word'][0],
                nickname=wx_json['nickname'][0],
                medianame=wx_json['medianame'][0],
                title=wx_json['title'][0],
                content=wx_json['content'][0],
                content_time=wx_json['content_time'][0],
                content_link=wx_json['content_link'][0]
            )
            db.session.add(wx_vo)
            db.session.commit()
            return jsonify({'code': 200, 'result': 'Succeess'})
        else:
            return jsonify({'code': 200, 'result': 'Weixin already existed.'})
    except Exception, e:
        print e
        return jsonify({'code': 400, 'result': str(e)})

@api.route('/wx/get_crawler_word/<string:size>')
def get_wx_crawler_word(size):
    key_words = []
    size = int(size)
    word_list = WeixinIndustryKeyWord.query.all()
    for record in word_list:
        last_fetch_time = record.update_time
        if last_fetch_time is None or time_proc.get_delta_date(last_fetch_time) > 0:
            key_words.append(record.to_json())
            record.fetching_tag = '1'
            record.update_time = time_proc.get_current_date()
            db.session.commit()
            size -= 1
            if size == 0:
                break
    if size > 0:
        for record in word_list:
            fetching_tag = record.fetching_tag
            if fetching_tag is None or fetching_tag == '0':
                key_words.append(record.to_json())
                record.fetching_tag = '1'
                record.update_time = time_proc.get_current_date()
                db.session.commit()
                size -= 1
                if size == 0:
                    break
    res = {
        'data': key_words,
        'code': 200,
        'msg': 'success'
    }
    return jsonify(res)


@api.route('/wx/update_crawler_word/<string:word>')
def update_wx_crawler_word(word):
    for record in list(WeixinIndustryKeyWord.query.filter_by(key_word=word).all()):
        record.update_time = time_proc.get_current_date()
        record.fetching_tag = '0'
        db.session.commit()
    return jsonify({'code': 'success'})