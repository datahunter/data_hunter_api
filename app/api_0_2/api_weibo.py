# coding=utf-8
__author__ = 'BorYee'

from flask import jsonify, request

from . import api
from app.models import WeiboUser
from app.models import UserWeibo
from app.models import WeiboUserInfo
from app.models.weibo import Weibo
from app.models.industry_keyword_model import WeiboIndustryKeyWord
from app import db
from app.utils import time_proc


@api.route('/weibo/add_user/', methods=['POST'])
def add_user():
    """
    这个API宝宝是用来接收爬虫端传来的微博用户名和链接的。
    """
    try:
        weibo_user = dict(request.form)
        for key in weibo_user:
            print key, weibo_user[key]
        if list(WeiboUser.query.filter_by(
                link=weibo_user['link'][0]
        ).all()
        ).__len__() == 0:
            weibo_user_vo = WeiboUser(
                nickname=weibo_user['nickname'][0],
                link=weibo_user['link'][0]
            )
            db.session.add(weibo_user_vo)
            db.session.commit()
            return jsonify({'code': 200, 'result': 'Added successful!'})
        else:
            return jsonify({'code': 200, 'result': 'User has already existed!'})
    except Exception, e:
        return jsonify({'code': 400, 'result': str(e)})


@api.route('/weibo/get_crawler_user/<string:size>')
def get_user(size):
    """
    这个API宝宝是用来响应爬虫端找用户的请求的。
    :param size: string
        爬虫端要多少个用户
    """
    size = int(size)
    user = []
    user_list = WeiboUser.query.all()
    for record in user_list:
        if size == 0:
            break
        if record.update_time is None or time_proc.get_delta_date(record.update_time) > 0:
            user.append(record.to_json())
            record.fetching_tag = 1
            record.update_time = time_proc.get_current_date()
            db.session.commit()
            size -= 1
    for record in user_list:
        if size == 0:
            break
        if record.fetching_tag is None or record.fetching_tag == '0':
            user.append(record.to_json())
            record.fetching_tag = 1
            record.update_time = time_proc.get_current_date()
            db.session.commit()
            size -= 1
    return jsonify({
        'data': user,
        'code': 200,
        'result': 'successful.'
    })


@api.route('/weibo/update_crawler_user/<string:id>')
def update_user(id):
    for record in WeiboUser.query.filter_by(id=id).all():
        record.update_time = time_proc.get_current_date()
        record.fetching_tag = 0
        db.session.commit()
    return jsonify({'code': 200, 'result': 'successful.'})


@api.route('/weibo/post_user_weibo/', methods=['POST'])
def add_user_weibo():
    try:
        weibo_json = dict(request.form)
        for key, value in weibo_json.iteritems():
            print key, value[0]
        if list(
                UserWeibo.query.filter_by(
                        uid=weibo_json['uid'][0],
                        content=weibo_json['content'][0],
                        content_time=weibo_json['content_time'][0]
                ).all()
        ).__len__() == 0:
            user_weibo_vo = UserWeibo(
                uid=weibo_json['uid'][0],
                content=weibo_json['content'][0],
                content_link=weibo_json['content_link'][0],
                content_time=weibo_json['content_time'][0],
                source=weibo_json['source'][0],
                repost_count=weibo_json['repost_count'][0],
                comment_count=weibo_json['comment_count'][0],
                like_count=weibo_json['like_count'][0]
            )
            db.session.add(user_weibo_vo)
            db.session.commit()
            return jsonify({'code': 200, 'result': 'Successful'})
        else:
            return jsonify({'code': 200, 'result': 'Weibo has already existed.'})
    except Exception, e:
        print e
        return jsonify({'code': 400, 'result': str(e)})


@api.route('/weibo/post_user_info/', methods=['POST'])
def add_user_info():
    try:
        info_json = dict(request.form)
        if list(
                WeiboUserInfo.query.filter_by(
                        link=info_json['link'][0]
                ).all()
        ).__len__() == 0:
            info_vo = WeiboUserInfo(
                nickname=info_json['nickname'][0],
                link=info_json['link'][0],
                address=info_json['address'][0],
                sex=info_json['sex'][0],
                birthday=info_json['birthday'][0],
                college=info_json['college'][0],
                college_year=info_json['college_year'][0],
                high_school=info_json['high_school'][0],
                high_school_year=info_json['high_school_year'][0],
                middle_school=info_json['middle_school'][0],
                middle_school_year=info_json['middle_school_year'][0],
                primary_school=info_json['primary_school'][0],
                primary_school_year=info_json['primary_school_year'][0],
                company=info_json['company'][0],
                company_address=info_json['company_address'][0],
                tags=info_json['tags'][0],
                photo=info_json['photo'][0]
            )
            db.session.add(info_vo)
            db.session.commit()
            return jsonify({'code': 200, 'result': 'Successful'})
        else:
            return jsonify({'code': 200, 'result': 'User has already existed.'})
    except Exception, e:
        return jsonify({'code': 400, 'result': str(e)})


@api.route('/weibo/industry/<string:industry>')
def query_weibo_by_industry(industry):
    weibo_json_list = []
    for weibo in Weibo.query.filter_by(industry=industry).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code': 200, 'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/weibo/sub_industry/<string:sub_industry>')
def query_by_sub_industry(sub_industry):
    weibo_json_list = []
    for weibo in Weibo.query.filter_by(sub_industry=sub_industry).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code': 200, 'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/weibo/key_word/<string:keyword>')
def query_by_key_word(keyword):
    weibo_json_list = []
    for weibo in Weibo.query.filter_by(key_word=keyword).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code': 200, 'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/weibo/search_weibo/<string:word>')
def search_weibo(word):
    weibo_json_list = []
    for weibo in Weibo.query.filter(Weibo.content.like('%' + word + '%')).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code': 200, 'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/weibo/post_weibo/', methods=['POST'])
def add_weibo():
    try:

        weibo_json = dict(request.form)
        if list(Weibo.query.filter(
                Weibo.content.like('%' + weibo_json['content'][0] + '%'),
                Weibo.user_link.like('%' + weibo_json['user_link'][0] + '%'),
                Weibo.content_time.like('%' + weibo_json['content_time'][0] + '%')
        ).all()).__len__() == 0:
            weibo_vo = Weibo(
                industry=weibo_json['industry'][0],
                sub_industry=weibo_json['sub_industry'][0],
                key_word=weibo_json['key_word'][0],
                nickname=weibo_json['nickname'][0],
                content=weibo_json['content'][0],
                user_link=weibo_json['user_link'][0],
                content_link=weibo_json['content_link'][0],
                content_time=weibo_json['content_time'][0],
                repost=weibo_json['repost'][0],
                comment=weibo_json['comment'][0],
                like_count=weibo_json['like_count'][0]
            )
            db.session.add(weibo_vo)
            db.session.commit()
            return jsonify({'code': 200, 'result': 'Success'})
        else:
            return jsonify({'code': 200, 'result': 'Weibo already existed.'})

    except Exception, e:
        print e
        return jsonify({'code': 400, 'result': str(e)})


@api.route('/industry/')
def industry_keywords():
    industry_list = []
    for record in WeiboIndustryKeyWord.query.all():
        industry_list.append(record.to_json())
    res = {
        'data': industry_list,
        'code': 200,
        'msg': 'success'
    }
    return jsonify(res)


@api.route('/weibo/add_keyword/', methods=['POST'])
def add_keyword():
    keyword_json = dict(request.form)
    if list(WeiboIndustryKeyWord.query.filter_by(industry=keyword_json['industry'],
                                                 sub_industry=keyword_json['sub_industry'],
                                                 key_word=keyword_json['key_word']).all()).__len__() == 0:
        keyword = WeiboIndustryKeyWord(industry=keyword_json['industry'], sub_industry=keyword_json['sub_industry'],
                                       key_word=keyword_json['key_word'])
        db.session.add(keyword)
        db.session.commit()
    return jsonify({'code': 200})


@api.route('/weibo/get_crawler_word/<string:size>')
def get_crawler_word(size):
    key_words = []
    size = int(size)
    word_list = WeiboIndustryKeyWord.query.all()
    for record in word_list:
        last_fetch_time = record.update_time
        if last_fetch_time is None or time_proc.get_delta_date(last_fetch_time) > 0:
            key_words.append(record.to_json())
            record.fetching_tag = '1'
            record.update_time = time_proc.get_current_date()
            db.session.commit()
            size -= 1
            if size == 0:
                break
    if size > 0:
        for record in word_list:
            fetching_tag = record.fetching_tag
            if fetching_tag is None or fetching_tag == '0':
                key_words.append(record.to_json())
                record.fetching_tag = '1'
                record.update_time = time_proc.get_current_date()
                db.session.commit()
                size -= 1
                if size == 0:
                    break
    res = {
        'data': key_words,
        'code': 200,
        'msg': 'success'
    }
    return jsonify(res)


@api.route('/weibo/update_crawler_word/<string:word>')
def update_crawler_word(word):
    for record in list(WeiboIndustryKeyWord.query.filter_by(key_word=word).all()):
        record.update_time = time_proc.get_current_date()
        record.fetching_tag = '0'
        db.session.commit()
    return jsonify({'code': 'success'})


