__author__ = 'BorYee'

from flask import request, jsonify
from . import api
from app.algorithms import social_media_polarity_analysis
from app.models import Weibo
from app.models import IndustryWeiboPolarity
from app import db

@api.route('/polarity/weibo/', methods=['POST', 'GET'])
def weibo_polarity():
    # TODO Should add Async I/O processing
    # Dangerous!
    sba = social_media_polarity_analysis.SnowNLPBaseAnalysis()
    for weibo in Weibo.query.filter_by(polarity_tag=None).all():
        weibo.polarity_tag = 1
        db.session.commit()
        iwp = IndustryWeiboPolarity(
            wid=weibo.id,
            polarity_probability=sba.predict_probability(weibo.content)
        )
        db.session.add(iwp)
        db.session.commit()
    return jsonify({'code': 200})
