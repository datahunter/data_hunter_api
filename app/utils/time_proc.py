__author__ = 'BorYee'

import time
import datetime


def get_current_date():
    return time.strftime('%Y-%m-%d', time.localtime())


def get_delta_date(proc_date):
    current_date = get_current_date()
    current_date = time.strptime(current_date, '%Y-%m-%d')
    current_date = datetime.datetime(current_date[0], current_date[1], current_date[2])
    proc_date = time.strptime(proc_date, '%Y-%m-%d')
    proc_date = datetime.datetime(proc_date[0], proc_date[1], proc_date[2])
    return (current_date - proc_date).days


if __name__ == '__main__':
    print get_delta_date('2016-01-01')