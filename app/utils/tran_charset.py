__author__ = 'BorYee'


def tran_unicode_2_utf8(src):
    if isinstance(src, unicode):
        return src.encode('utf-8', 'ignore')
    else:
        return src