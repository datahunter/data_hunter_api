# coding=utf-8
__author__ = 'BorYee'

import jieba
import jieba.posseg as posseg
from snownlp import SnowNLP


class PolarityAnalysis():
    training_X = []
    training_y = []

    def judge_data(self, X):
        src = ''
        if isinstance(X, str) or isinstance(X, unicode):
            if X.__len__() > 0:
                src = X
        return src

    def set_training_set(self, X, y):
        if type(X) is list and type(y) is list and list(X).__len__() > 0 and list(X).__len__() == list(y).__len__():
            self.training_X = X[:]
            self.training_y = y[:]

    def fit(self, X=None, y=None):
        pass

    def predict_probability(self, X):
        pass

    def predict(self, X):
        pass


class SnowNLPBaseAnalysis(PolarityAnalysis):
    def __init__(self):
        pass

    def judge_data(self, X):
        src = ''
        if isinstance(X, unicode) and X.__len__() > 0:
            src = X
        elif isinstance(X, str) and X.__len__() > 0:
            src = X.decode('utf-8')
        return src

    def predict_probability(self, X):
        X = self.judge_data(X)
        s = SnowNLP(X)
        return 1 - float(s.sentiments)

    def predict(self, X):
        y = 0.0
        probability = self.predict_probability(X)
        if probability > 0.5:
            y = 1
        else:
            y = 0
        return y


class SentimentWordFrequencyBasedAnalysis(PolarityAnalysis):
    sentiment_word_dictionary = ''

    def __init__(self, sentiment_word_dictionary):
        self.sentiment_word_dictionary = sentiment_word_dictionary

        jieba.load_userdict(self.sentiment_word_dictionary)


    def predict_probability(self, X):
        X = self.judge_data(X)
        X = posseg.cut(X, HMM=False)
        pos = 0
        neg = 0
        for word, flag in X:
            if flag == 'pos':
                pos += 1
            elif flag == 'neg':
                neg += 1
        if pos != 0 or neg != 0:
            y = neg / float(pos + neg)
        else:
            y = 0.0
        return y


    def predict(self, X):
        y = 0.0
        probability = self.predict_probability(X)
        if probability > 0.5:
            y = 1
        else:
            y = 0
        return y


if __name__ == '__main__':
    swfpa = SentimentWordFrequencyBasedAnalysis('../res/dict_jieba.txt')
    s = '看个美人鱼，还把信用卡整丢了[笑cry][笑cry][笑cry]在银河里找了半天巴黎贝甜，结果一问没有[拜拜][拜拜]你个死大众点评！还好挂失新开不用花钱[哈欠][哈欠]回来打不到车，简直冻尿了[打脸][打脸][打脸]'
    print swfpa.predict_probability(s)
