__author__ = 'BorYee'

from app import db


class WeiboUser(db.Model):
    __tablename__ = 'tb_weibo_user'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    nickname = db.Column(db.TEXT)
    link = db.Column(db.TEXT)
    fetching_tag = db.Column(db.VARCHAR)
    update_time = db.Column(db.VARCHAR)


    def to_json(self):
        return {
            'id': self.id,
            'nickname': self.nickname,
            'link': self.link,
            'fetching_tag': self.fetching_tag,
            'update_time': self.update_time
        }

    def __repr__(self):
        return '<WeiboUser %s>' % self.nickname
