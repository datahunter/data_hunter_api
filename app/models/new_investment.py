__author__ = 'BorYee'

from app import db


class NewInvestment(db.Model):
    __tablename__ = 'tb_new_investment'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    start_up_name = db.Column(db.VARCHAR)
    area = db.Column(db.VARCHAR)
    location = db.Column(db.VARCHAR)
    invest_time = db.Column(db.VARCHAR)
    amount = db.Column(db.VARCHAR)
    invest_firm_name = db.Column(db.VARCHAR)
    round = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id': self.id,
            'start_up_name': self.start_up_name,
            'area': self.area,
            'location': self.location,
            'invest_time': self.invest_time,
            'amount': self.amount,
            'invest_firm_name': self.invest_firm_name,
            'round':self.round
        }