__author__ = 'BorYee'

from app import db


class StartupLink(db.Model):
    __tablename__ = 'tb_start_up_link'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    start_up_name = db.Column(db.VARCHAR)
    start_up_link = db.Column(db.TEXT)
    source = db.Column(db.VARCHAR)
    fetch_tag = db.Column(db.CHAR)
    fetch_date = db.Column(db.CHAR)

    def to_json(self):
        return {
            'id': self.id,
            'start_up_name': self.start_up_name,
            'start_up_link': self.start_up_link,
            'source': self.source,
            'fetch_tag': self.fetch_tag,
            'fetch_date': self.fetch_date
        }