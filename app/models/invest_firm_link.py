__author__ = 'BorYee'

from app import db


class InvestFirmLink(db.Model):
    __tablename__ = 'tb_invest_firm_link'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    invest_firm_name = db.Column(db.VARCHAR)
    invest_firm_link = db.Column(db.TEXT)
    source = db.Column(db.VARCHAR)
    fetch_tag = db.Column(db.CHAR)
    fetch_date = db.Column(db.CHAR)

    def to_json(self):
        return {
            'id': self.id,
            'invest_firm_name': self.invest_firm_name,
            'invest_firm_link': self.invest_firm_link,
            'source': self.source,
            'fetch_tag': self.fetch_tag,
            'fetch_date': self.fetch_date
        }