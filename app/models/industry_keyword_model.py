__author__ = 'BorYee'

from .. import db


class WeiboIndustryKeyWord(db.Model):
    __tablename__ = 'tb_industry'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    industry = db.Column(db.VARCHAR)
    sub_industry = db.Column(db.VARCHAR)
    key_word = db.Column(db.VARCHAR)
    update_time = db.Column(db.VARCHAR)
    fetching_tag = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id': self.id,
            'industry': self.industry,
            'sub_industry': self.sub_industry,
            'key_word': self.key_word,
            'update_time': self.update_time,
            'fetching_tag': self.fetching_tag
        }


    def __repr__(self):
        return '<weibo %s>' % self.content


class WeixinIndustryKeyWord(db.Model):
    __tablename__ = 'tb_wx_industry'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    industry = db.Column(db.VARCHAR)
    sub_industry = db.Column(db.VARCHAR)
    key_word = db.Column(db.VARCHAR)
    update_time = db.Column(db.VARCHAR)
    fetching_tag = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id': self.id,
            'industry': self.industry,
            'sub_industry': self.sub_industry,
            'key_word': self.key_word,
            'update_time': self.update_time,
            'fetching_tag': self.fetching_tag
        }


    def __repr__(self):
        return '<weixin %s>' % self.content