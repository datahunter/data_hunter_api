__author__ = 'BorYee'

from app import db


class StartupInfo(db.Model):
    __tablename__ = 'tb_start_up_info'

    id = db.Column(db.INT, primary_key=True)
    lid = db.Column(db.INT, primary_key=True, autoincrement=True)
    start_up_name = db.Column(db.VARCHAR)
    area = db.Column(db.VARCHAR)
    sub_area = db.Column(db.VARCHAR)
    location = db.Column(db.VARCHAR)
    sub_location = db.Column(db.VARCHAR)
    official_link = db.Column(db.TEXT)
    introduction = db.Column(db.TEXT)
    full_name = db.Column(db.TEXT)
    found_date = db.Column(db.VARCHAR)
    status = db.Column(db.VARCHAR)
    address = db.Column(db.TEXT)
    mail = db.Column(db.TEXT)
    phone = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id': self.id,
            'lid': self.lid,
            'start_up_name': self.start_up_name,
            'area': self.area,
            'sub_area': self.sub_area,
            'location': self.location,
            'sub_location': self.sub_location,
            'official_link': self.official_link,
            'introduction': self.introduction,
            'full_name': self.full_name,
            'found_date': self.found_date,
            'status': self.status,
            'address': self.address,
            'mail': self.mail,
            'phone': self.phone
        }