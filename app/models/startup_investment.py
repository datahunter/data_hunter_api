__author__ = 'BorYee'

from app import db


class StartupInvestment(db.Model):
    __tablename__ = 'tb_start_up_investment'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    sid = db.Column(db.INT)
    invest_firm_name = db.Column(db.VARCHAR)
    invest_time = db.Column(db.VARCHAR)
    invest_round = db.Column(db.VARCHAR)
    amount = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id':self.id,
            'sid':self.sid,
            'invest_firm_name':self.invest_firm_name,
            'invest_time':self.invest_time,
            'invest_round':self.invest_round,
            'amount':self.amount
        }