__author__ = 'BorYee'

from app import db


class Weixin(db.Model):
    __tablename__ = 'tb_industry_wx'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    industry = db.Column(db.VARCHAR)
    sub_industry = db.Column(db.VARCHAR)
    key_word = db.Column(db.VARCHAR)
    nickname = db.Column(db.TEXT)
    medianame = db.Column(db.TEXT)
    title = db.Column(db.TEXT)
    content = db.Column(db.TEXT)
    content_time = db.Column(db.VARCHAR)
    content_link = db.Column(db.TEXT)

    def tojson(self):
        return {
            'id': self.id,
            'industry': self.industry,
            'sub_industry': self.sub_industry,
            'key_word': self.key_word,
            'nickname': self.nickname,
            'medianame': self.medianame,
            'title': self.title,
            'content': self.content,
            'content_time': self.content_time,
            'content_link': self.content_link
        }

    def __repr__(self):
        return '<weixin %s>' % self.content