__author__ = 'BorYee'

from app import db


class News(db.Model):
    __tablename__ = 'tb_news'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    title = db.Column(db.TEXT)
    content = db.Column(db.TEXT)
    news_url = db.Column(db.TEXT)
    publish_time = db.Column(db.VARCHAR)
    source = db.Column(db.TEXT)

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title,
            'content': self.content,
            'news_url': self.news_url,
            'publish_time': self.publish_time,
            'source': self.source
        }

    def __repr__(self):
        return '<weibo %s>' % self.content