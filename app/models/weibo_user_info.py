__author__ = 'BorYee'

from app import db


class WeiboUserInfo(db.Model):
    __tablename__ = 'tb_weibo_user_info'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    nickname = db.Column(db.TEXT)
    link = db.Column(db.TEXT)
    address = db.Column(db.TEXT)
    sex = db.Column(db.VARCHAR)
    birthday = db.Column(db.VARCHAR)
    college = db.Column(db.VARCHAR)
    college_year = db.Column(db.VARCHAR)
    high_school = db.Column(db.VARCHAR)
    high_school_year = db.Column(db.VARCHAR)
    middle_school = db.Column(db.VARCHAR)
    middle_school_year = db.Column(db.VARCHAR)
    primary_school = db.Column(db.VARCHAR)
    primary_school_year = db.Column(db.VARCHAR)
    company = db.Column(db.TEXT)
    company_address = db.Column(db.TEXT)
    tags = db.Column(db.TEXT)
    photo = db.Column(db.TEXT)

    def to_json(self):
        return {
            'id': self.id,
            'nickname': self.nickname,
            'link': self.link,
            'address': self.address,
            'sex': self.sex,
            'birthday': self.birthday,
            'college': self.college,
            'college_year': self.college_year,
            'high_school': self.high_school,
            'high_school_year': self.high_school_year,
            'middle_school': self.middle_school,
            'middle_school_year': self.middle_school_year,
            'primary_school': self.primary_school,
            'primary_school_year': self.primary_school_year,
            'company': self.company,
            'company_address': self.company_address,
            'tags': self.tags,
            'photo': self.photo
        }

    def __repr__(self):
        return '<WeiboUserInfo %s>' % self.nickname