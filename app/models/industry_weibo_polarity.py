__author__ = 'BorYee'

from app import db


class IndustryWeiboPolarity(db.Model):
    __tablename__ = 'tb_industry_weibo_pa'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    wid = db.Column(db.INT)
    polarity_probability = db.Column(db.FLOAT)

    def to_json(self):
        return {
            'id': self.id,
            'wid': self.wid,
            'polarity_probability': self.polarity_probability
        }


    def __repr__(self):
        return (self.wid, self.polarity_probability)

