__author__ = 'BorYee'

from app import db


class FounderLink(db.Model):
    __tablename__ = 'tb_founder_link'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    sid = db.Column(db.INT)
    founder_name = db.Column(db.VARCHAR)
    founder_link = db.Column(db.TEXT)
    source = db.Column(db.VARCHAR)
    fetch_tag = db.Column(db.CHAR)
    fetch_date = db.Column(db.CHAR)

    def to_json(self):
        return {
            'id':self.id,
            'sid':self.sid,
            'founder_name':self.founder_name,
            'founder_link':self.founder_link,
            'fetch_tag':self.fetch_tag,
            'fetch_date':self.fetch_date
        }