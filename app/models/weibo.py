__author__ = 'BorYee'

from .. import db


class Weibo(db.Model):
    __tablename__ = 'tb_industry_weibo'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    industry = db.Column(db.VARCHAR)
    sub_industry = db.Column(db.VARCHAR)
    key_word = db.Column(db.VARCHAR)
    nickname = db.Column(db.TEXT)
    content = db.Column(db.TEXT)
    user_link = db.Column(db.TEXT)
    content_link = db.Column(db.TEXT)
    content_time = db.Column(db.VARCHAR)
    repost = db.Column(db.VARCHAR)
    comment = db.Column(db.VARCHAR)
    like_count = db.Column(db.VARCHAR)
    polarity_tag = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id': self.id,
            'industry': self.industry,
            'sub_industry': self.sub_industry,
            'key_word': self.key_word,
            'nickname': self.nickname,
            'content': self.content,
            'user_link': self.user_link,
            'content_link': self.content_link,
            'content_time': self.content_time,
            'repost': self.repost,
            'comment': self.comment,
            'like_count': self.like_count,
            'polarity_tag': self.polarity_tag
        }

    def __repr__(self):
        return '<weibo %s>' % self.content