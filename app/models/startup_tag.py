__author__ = 'BorYee'

from app import db


class StartupTag(db.Model):
    __tablename__ = 'tb_start_up_tag'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    sid = db.Column(db.INT)
    tag = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id': self.id,
            'sid': self.sid,
            'tag': self.tag
        }