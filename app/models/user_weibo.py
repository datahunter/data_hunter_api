__author__ = 'BorYee'

from app import db


class UserWeibo(db.Model):
    __tablename__ = 'tb_user_weibo'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    uid = db.Column(db.INT)
    content = db.Column(db.TEXT)
    weibo_type = db.Column(db.VARCHAR)
    content_link = db.Column(db.TEXT)
    content_time = db.Column(db.VARCHAR)
    content_topic = db.Column(db.TEXT)
    source = db.Column(db.VARCHAR)
    repost_count = db.Column(db.VARCHAR)
    comment_count = db.Column(db.VARCHAR)
    like_count = db.Column(db.VARCHAR)

    def to_json(self):
        return {
            'id': self.id,
            'uid': self.uid,
            'content': self.content,
            'weibo_type': self.weibo_type,
            'content_link': self.content_link,
            'content_time': self.content_time,
            'content_topic': self.content_topic,
            'source': self.source,
            'repost_count': self.repost_count,
            'comment_count': self.comment_count,
            'like_count': self.like_count
        }

    def __repr__(self):
        return '<UserWeibo %s>' % self.content