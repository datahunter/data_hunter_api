CREATE TABLE tb_new_investment(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  start_up_name VARCHAR(45),
  area  VARCHAR(45),
  location  VARCHAR(45),
  invest_time  VARCHAR(45),
  amount  VARCHAR(45),
  invest_firm_name  VARCHAR(45),
  round VARCHAR(45)
)DEFAULT CHARSET=utf8;