CREATE TABLE tb_invest_firm_round(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  fid INT,
  round VARCHAR(45)
);