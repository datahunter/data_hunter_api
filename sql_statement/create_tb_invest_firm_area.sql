CREATE TABLE tb_invest_firm_area(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  fid INT,
  area VARCHAR(45),
);