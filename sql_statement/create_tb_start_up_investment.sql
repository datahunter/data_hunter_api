CREATE TABLE tb_start_up_investment(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
sid INT,
invest_firm_name VARCHAR(45),
invest_time VARCHAR(10),
invest_round VARCHAR(10),
amount VARCHAR(20)
)DEFAULT CHARSET=utf8;