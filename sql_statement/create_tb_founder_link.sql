CREATE TABLE tb_founder_link(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  sid INT,
  founder_name VARCHAR(45),
  founder_link TEXT,
  source VARCHAR(45),
  fetch_tag  CHAR(1),
  fetch_date CHAR(13)
)DEFAULT CHARSET=utf8;