CREATE TABLE tb_founder_experience(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  fid INT,
  institute_name VARCHAR(45),
  experience_type VARCHAR(45)
);