USE db_data_hunter;
CREATE TABLE tb_news_link (
  id         INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  url        TEXT,
  source     VARCHAR(45),
  fetch_tag  CHAR(1),
  fetch_date CHAR(13)
)DEFAULT CHARSET=utf8;