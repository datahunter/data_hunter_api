CREATE TABLE tb_start_up_info(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
lid INT ,
start_up_name VARCHAR(45),
area VARCHAR(45),
sub_area VARCHAR(45),
location VARCHAR(45),
sub_location VARCHAR(45),
official_link TEXT,
introduction TEXT,
full_name TEXT,
found_date VARCHAR(10),
status VARCHAR(10),
address TEXT,
mail TEXT,
phone VARCHAR(20)
)DEFAULT CHARSET=utf8;