CREATE TABLE tb_invest_firm_info(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  lid INT,
  invest_firm_name VARCHAR(45),
  official_link TEXT,
  introduction TEXT
):