USE db_data_hunter;
CREATE TABLE tb_news (
  id           INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  lid          INT NOT NULL,
  title        TEXT,
  abstract     TEXT,
  content      TEXT,
  industry     VARCHAR(45),
  sub_industry VARCHAR(45)
)DEFAULT CHARSET=utf8;