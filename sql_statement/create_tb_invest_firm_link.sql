CREATE TABLE tb_invest_firm_link(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  invest_firm_name VARCHAR(45),
  invest_firm_link TEXT,
  source VARCHAR(45),
  fetch_tag  CHAR(1),
  fetch_date CHAR(13)
)DEFAULT CHARSET=utf8;