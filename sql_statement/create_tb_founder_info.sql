CREATE TABLE tb_founder_info(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  lid INT ,
  founder_name VARCHAR(45),
  company_name VARCHAR(45),
  title VARCHAR(45),
  location VARCHAR(45),
  sub_location VARCHAR(45),
  introduction TEXT,
);