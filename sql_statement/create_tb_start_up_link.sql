CREATE TABLE tb_start_up_link(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  start_up_name VARCHAR(45),
  start_up_link TEXT,
  source VARCHAR(45),
  fetch_tag  CHAR(1),
  fetch_date CHAR(13)
)DEFAULT CHARSET=utf8;