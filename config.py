__author__ = 'BorYee'


class Config():
    SECRET_KEY = 'Data@Hunter*Stadio-BorYee'
    SSL_DISABLE = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_RECORD_QUERIES = True

    @staticmethod
    def init_app(app):
        pass


class TestConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/db_data_hunter'


class CompanyTestConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://root:123456@localhost/db_data_hunter'


config = {
    'test': TestConfig,
    'company': CompanyTestConfig
}